# Provide tar'ed sources to the target image (in /usr/src/)

LINUX_SRC_NAME = "linux-src"
LINUX_SRC_DIR = "/usr/src"
LINUX_SRC = "${LINUX_SRC_DIR}/${LINUX_SRC_NAME}"

DEPENDS:append = "rsync-native"

do_install[cleandirs] = "${D}/${LINUX_SRC} ${D}/${LINUX_SRC_DIR}/kernel"
do_install() {
	install -d ${D}/${LINUX_SRC_DIR}/kernel
	install -d ${D}/${LINUX_SRC}
	rsync -Hax -L --exclude ".git" --exclude ".github" --exclude ".kernel-meta" ${S}/ ${D}/${LINUX_SRC}
	rsync -Hax -L --exclude ".git" --exclude ".github" --exclude ".kernel-meta" ${B}/.config ${D}/${LINUX_SRC}/defconfig

	cd ${D}/${LINUX_SRC_DIR}
	tar -czf ${LINUX_SRC_NAME}.tar.gz -C ./${LINUX_SRC_NAME} .
	rm -rf ${D}/${LINUX_SRC}
}

FILES:${PN} = "${LINUX_SRC_DIR}"
