require recipes-extended/images/core-image-full-cmdline.bb

inherit image-buildinfo

DESCRIPTION = "Stresstests image"
IMAGE_FEATURES += "ssh-server-openssh \
	       	   tools-sdk \
		   tools-debug"

IMAGE_INSTALL:append = "\
     stresstest \
     openssl-dev \
     dtc \
     zile \
     htop \
     ntpdate \
"

# Extra 2 GiB of free space
IMAGE_ROOTFS_EXTRA_SPACE = "2097152"
