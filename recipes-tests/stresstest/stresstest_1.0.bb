FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/${PN}/tests:"

LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI += "file://stress_test.sh"
SRC_URI += "file://DIS_cpu100_1.sh"
SRC_URI += "file://mon_board.sh"
SRC_URI += "file://memtester.sh"
SRC_URI += "file://xenomai_scary_grinder.sh"

DEPENDS += "python3"
RDEPENDS:${PN} += "bc kernel-devsrc memtester stress"

do_install() {
	install -m 0755 -d ${D}/usr/src/${PN}
	install -m 0755 -d ${D}/usr/src/${PN}/tests

	install -m 0755 ${WORKDIR}/stress_test.sh ${D}/usr/src/${PN}

	install -m 0755 ${WORKDIR}/DIS_cpu100_1.sh ${D}/usr/src/${PN}/tests
	install -m 0755 ${WORKDIR}/mon_board.sh ${D}/usr/src/${PN}/tests
	install -m 0755 ${WORKDIR}/memtester.sh ${D}/usr/src/${PN}/tests
	install -m 0755 ${WORKDIR}/xenomai_scary_grinder.sh ${D}/usr/src/${PN}/tests
}

FILES:${PN} += " \
	/usr/src/${PN} \
	/usr/src/${PN}/tests \
"
