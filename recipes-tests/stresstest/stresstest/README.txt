Kernel stress tests
===================

1. Preparation:

1.1 It is best to have:
	- rootfs mounted via NFS (ETH) [1] or use UBIFS on NAND or NOR
	- insert usb memory to the port [2]

1.2 Deploy OE/yocto on your machine (with KAS) and build core-image-*
cd <yocto deploy dir>
source  ./poky/oe-init-build-env
bitbake core-image-*

1.3 Setup the build image on the device under test (DUT)

1.4 Run "cd /usr/src/stresstest; ./stress_test.sh"
to inspect usage information

2. Please add all your test scripts (or programs) to
/usr/src/stresstest/tests [1]

They will be run when ./stresstest.sh is invoked

3. It is possible to disable test execution (stored in [1])
by renaming it to start with "DIS_" (e.g. foo.sh -> DIS_foo.sh)

4. It is possible to log to target (via e.g. ssh 192.168.Y.X) when
tests are running and inspect the state of the board (by calling
e.g. 'top' utility or 'ps')

To observe the "build" progress: "watch -n10 du -sh /mnt/sda1/build/"



NOTE:
-----
[1] - exportfs -> using NFS file systems

- Host PC side (please make sure that NFS kernel support is compiled into
the Linux kernel - or module is provided):
https://www.linode.com/docs/networking/basic-nfs-configuration-on-debian-7

On my PC
root@jawa:/srv/tftp/XXX/rootfs/usr/# cat /etc/exports
/srv/tftp/XXX/rootfs *(rw,sync,no_subtree_check,no_root_squash)


- On the target:
"run boot_nfs" in u-boot prompt (some adjustments may be needed).

[2] - Due to using linux kernel build system as stress test build code -
it is recommended to use file systems supporting symbolic links (e.g.
ext[234]/UBIFS). This setup will not work with devices having FAT FS on them
(the script itself will fail).
