#!/bin/sh

MEM_SIZE="256"
ITER="16"
LOG_FILE="/var/log/memtester.log"

> ${LOG_FILE}

exec 1>${LOG_FILE}
exec 2>&1

echo "Memtester test program!"
echo "Begin: $(date)"
memtester ${MEM_SIZE} ${ITER}
echo ""
echo "End: $(date)"

exit 0
