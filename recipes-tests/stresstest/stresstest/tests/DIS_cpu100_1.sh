#!/bin/sh
# 100% CPU utilization script
#
# Copyright (C) 2018
# Lukasz Majewski, DENX Software Engineering, lukma@denx.de
#

gzip < /dev/urandom > /dev/null

exit 0
