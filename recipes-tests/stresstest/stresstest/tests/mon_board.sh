#!/bin/sh
# BUT monitoring script
#
# Copyright (C) 2018
# Lukasz Majewski, DENX Software Engineering, lukma@denx.de
#

set -eu

interval="5"

THERMAL_PATH="/sys/class/thermal/thermal_zone0"
FREQ_PATH="/sys/devices/system/cpu/cpu0/cpufreq/"

while :;
do
    temp=$(cat ${THERMAL_PATH}/temp)
    freq=$(cat ${FREQ_PATH}/cpuinfo_cur_freq)
    load=$(uptime | cut -d',' -f 3-5 | cut -d':' -f 2)
    mem=$(free --mega -w | grep Mem: | awk '{ print "Mem U/F: " $3"/"$4 " MiB" }')

    echo -ne "temp: ${temp} [m degC] freq: ${freq} [Hz] Load: ${load} ${mem}"\\r
    sleep ${interval}
done

exit 0
