#!/bin/sh
#
# Copyright (C) 2023
# Lukasz Majewski, DENX Software Engineering, lukma@denx.de
#

#set -x

# trap ctrl-c and call ctrl_c()
trap ctrl_c INT

function ctrl_c() {
    echo "** Trapped CTRL-C"
    cleanup_board ${DEV_AUTOMOUNT_DIR} ${USB_MEM}
    kill -KILL -${$}
}

update_timestamp() {
	find $1 -exec touch -a -m {} \;
}

echoErr() { echo "$@" 1>&2; exit 1; }
checkCmd() { command -v $1 >/dev/null 2>&1 || { echo >&2 "I "; exit 1; } }

spawnTests() {
    EXT_PIDS=()

    echo "Tests:";
    for t in ${ROOT_DIR}/tests/*;
    do
	if [ -f ${t} -a -x ${t} ]; then
	    tn=${t##*/}
	    # We skip test marked as "DIS_foo.sh"
	    grep -E -q "^DIS_" <<<${tn}
	    [ $? -eq 0 ] && continue

	    ${t} &
	    local pid=$!

	    printf "\t---> [${pid}] ${tn} \n"
	    EXT_PIDS+=("${pid}")
	fi
    done
}

killTests() {
    for pid in ${EXT_PIDS[@]}
    do
	for str in $(pgrep -P ${pid}); do
	    [ ! -z "${str}" ] && kill -TERM ${str} 2>/dev/null
	done
	kill -TERM ${pid} 2>/dev/null
	wait "${pid}" 2>/dev/null
    done
}

testFS() {
    stat -f -c %T ${1} | grep -q ${2}
    [ $? -ne 0 ] && { echoErr "Only ext[234] fs supported - ln -s problems";\
		exit 1; }
}

usage() {
    echo "" 1>&2
    echo -n "Usage: $0 [-c <loop count>] [-k <kernel src>] " 1>&2
    echo "[-s [<SD card>]|[-1 <No SD build>]] [-u <USB mem>]" 1>&2
    echo "" 1>&2
    exit 1
}

setup_linux_src() {
    [ ! -z "$(ls -A ${KERNEL_SOURCE})" ] && return

    if [ -f "${KERNEL_SRC_ARCH}" ]
    then
	echo -n "Empty ${KERNEL_SOURCE}, extract from ${KERNEL_SRC_ARCH}"
	tar -xzf ${KERNEL_SRC_ARCH} -C ${KERNEL_SOURCE}
	[ "$?" -eq 0 ] && echo " :OK" || exit 1
    fi
}

setup_board() {
    # Enable 'performance' governor
    echo performance > $(find /sys/ -name "scaling_governor")

    dir="${1}/${2}"

    [ -d ${dir} ] && rm -rf ${dir} > /dev/null 2>&1
    mkdir -p ${dir} > /dev/null 2>&1

    mount /dev/${2} ${dir} || exit $?

    # Create SD/eMMC card build dir
    [ "${SD_CARD}" != "-1" ] && [ -d ${SD_CARD} ] && rm -rf ${SD_CARD} > /dev/null 2>&1
    [ "${SD_CARD}" != "-1" ] && mkdir -p ${SD_CARD} > /dev/null 2>&1
}

cleanup_board() {
    dir="${1}/${2}"
    umount ${dir} > /dev/null 2>&1
    rm -rf ${dir} > /dev/null 2>&1

    [ "${SD_CARD}" != "-1" ] && rm -rf ${SD_CARD} > /dev/null 2>&1
}

KERNEL_SOURCE="/usr/src/kernel"
KERNEL_SRC_ARCH="/usr/src/linux-src.tar.gz"
DEV_AUTOMOUNT_DIR="/mnt"
SD_CARD="/mmcblk0p1"
USB_MEM="sda1"

tmp=$(readlink -f $0)
ROOT_DIR=${tmp%/*}

tests_count=1

while getopts "c:hk:s:u:" opt; do
    case $opt in
	c)
	    tests_count=${OPTARG}
	    ;;
	k)
	    KERNEL_SOURCE=${OPTARG}
	    ;;
	s)
	    SD_CARD=${OPTARG}
	    ;;
	u)
	    USB_MEM=${OPTARG}
	    ;;
	h)
	    usage
	    ;;
	*)
	    usage
	    ;;
    esac
done

setup_linux_src
setup_board ${DEV_AUTOMOUNT_DIR} ${USB_MEM}

kernFS=$(stat -f -c %T ${KERNEL_SOURCE})
[ "${SD_CARD}" != "-1" ] && sdmntFS=$(stat -f -c %T ${SD_CARD})
usbmntFS=$(stat -f -c %T ${DEV_AUTOMOUNT_DIR}/${USB_MEM})

echo "##############################################################"
echo "# Stress tests for RPI4 (XENOMAI 4)"
echo "##############################################################"
echo "#					"
printf "# kernel src:\tfs:${kernFS}\t\t${KERNEL_SOURCE}\n"
[ "${SD_CARD}" != "-1" ] && printf "# sd dir:\tfs:${sdmntFS}\t${SD_CARD}\n"
printf "# usb mnt:\tfs:${usbmntFS}\t${DEV_AUTOMOUNT_DIR}/${USB_MEM}\n"
echo "#"
echo "# Interations: ${tests_count}"
echo "#"

[ "${SD_CARD}" != "-1" ] && [ ! -d ${SD_CARD} ] && echoErr "SD/eMMC error"
[ ! -d ${DEV_AUTOMOUNT_DIR}/${USB_MEM} ] && echoErr "No USB memory inserted"
[ ! -d ${KERNEL_SOURCE}/mm ] && echoErr "No kernel source [${KERNEL_SOURCE}]"

checkCmd "stat"
checkCmd "grep"
checkCmd "gcc"

testFS ${DEV_AUTOMOUNT_DIR}/${USB_MEM} "ext[234]"
[ "${SD_CARD}" != "-1" ] && testFS ${SD_CARD} "ext[234]"


count=1
cur_dir=$(pwd)

cd ${KERNEL_SOURCE}

while :;
do
    echo -e "\r"
    echo "STRESS TEST LOOP No: ${count}"
    count=$((count+1))

    [ -d ${DEV_AUTOMOUNT_DIR}/${USB_MEM}/build ] && \
	rm -rf ${DEV_AUTOMOUNT_DIR}/${USB_MEM}/build
    [ "${SD_CARD}" != "-1" ] && [ -d ${SD_CARD}/build ] && rm -rf ${SD_CARD}/build

    mkdir ${DEV_AUTOMOUNT_DIR}/${USB_MEM}/build
    [ "${SD_CARD}" != "-1" ] && mkdir ${SD_CARD}/build

    make mrproper > /dev/null 2>&1
    cp defconfig .config
    ./scripts/config -e CONFIG_TRIM_UNUSED_KSYMS -d CONFIG_DEBUG_INFO
    make olddefconfig > /dev/null 2>&1

    cp .config ${DEV_AUTOMOUNT_DIR}/${USB_MEM}/build/.config
    [ "${SD_CARD}" != "-1" ] && cp .config ${SD_CARD}/build/.config

    make mrproper > /dev/null 2>&1

    spawnTests

    #( sleep 10 ) &
    start_usb=$(date +%s)
    make Image modules -j4 -s O=${DEV_AUTOMOUNT_DIR}/${USB_MEM}/build > /dev/null 2>&1 &
    usb_build_pid=$!
    taskset -pc 0-3 ${usb_build_pid} > /dev/null 2>&1

    #( sleep 20 ) &
    if [ "${SD_CARD}" != "-1" ]
    then
	start_sd=$(date +%s)
	make Image modules -j4 -s O=${SD_CARD}/build &
	sd_build_pid=$!
	taskset -pc 4-7 ${sd_build_pid} > /dev/null 2>&1
    fi

    echo "Builds (make):"
    printf "\t===> [${usb_build_pid}] USB:\t${USB_MEM}\n"
    [ "${SD_CARD}" != "-1" ] && printf "\t===> [${sd_build_pid}] eMMC/SD:\t${SD_CARD}\n"
    echo ""

    wait ${usb_build_pid}
    [ "${SD_CARD}" != "-1" ] && wait ${sd_build_pid}

    killTests

    end=$(date +%s)
    runtime_usb=$(($((end-start_usb))/60))
    [ "${SD_CARD}" != "-1" ] && runtime_sd=$(($((end-start_sd))/60))

    printf "\n\n"
    printf "\t===> [${usb_build_pid}] USB exec time:\t${runtime_usb} [min]\n"
    [ "${SD_CARD}" != "-1" ] && printf "\t===> [${sd_build_pid}] SD exec time:\t${runtime_sd} [min]\n"

    [ "${count}" -gt "${tests_count}" ] && break;
done;

echo -e "\r"
echo ""

cd ${cur_dir}
cleanup_board ${DEV_AUTOMOUNT_DIR} ${USB_MEM}

exit 0
